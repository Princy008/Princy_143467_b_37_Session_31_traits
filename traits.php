<?php
trait Hello{


    public function sayHello()
    {
        echo "Hello";

    }

}
trait World{
    public function sayWorld()
    {
        echo "World";
    }
}
trait Moon{
    use Hello,World;
}

class MyHelloWorld{
  use Moon;
  /*  public function sayExclamationMark()
    {
        echo "!";
    }*/
}
$ob=new MyHelloWorld();
$ob->sayHello();
$ob->sayWorld();
//$ob->sayExclamationMark();
?>